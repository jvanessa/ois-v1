import java.util.*; 

public class Gravitacija {
    
    public static void main (String[] args) {
        Scanner sc = new Scanner (System.in);
        
        float visina = sc.nextFloat();
        
        float pospesek = izracun(visina);
        System.out.println(pospesek);
    }
    
    public static float izracun (float v) {
        
        float C = 6673;
        long M = 597200000;
        float r = 6371000;
        
        return (C*(float)M)/((r + v) * (r + v)) * 100;
    }
    
    public void izpisi(int visina, int gravitacija) {
        System.out.println(visina);
        System.out.println(gravitacija);
    }
}